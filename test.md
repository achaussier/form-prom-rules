## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | 2.67.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.67.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_linux_virtual_machine.server](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/linux_virtual_machine) | resource |
| [azurerm_network_interface.nic01](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/network_interface) | resource |
| [azurerm_public_ip.pub_ip](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/public_ip) | resource |
| [azurerm_resource_group.rg](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/resource_group) | resource |
| [azurerm_subnet.private](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/subnet) | resource |
| [azurerm_virtual_network.network](https://registry.terraform.io/providers/hashicorp/azurerm/2.67.0/docs/resources/virtual_network) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_net_space"></a> [net\_space](#input\_net\_space) | Réseau IP géré par le réseau à créer | `string` | `"10.0.0.0/16"` | no |
| <a name="input_net_subnet"></a> [net\_subnet](#input\_net\_subnet) | Sous réseau dans lequel raccorder les instances | `string` | `"10.0.1.0/24"` | no |
| <a name="input_vm_name"></a> [vm\_name](#input\_vm\_name) | Nom de l'instance | `string` | n/a | yes |
| <a name="input_vm_size"></a> [vm\_size](#input\_vm\_size) | Type de l'instance | `string` | `"Standard_A1_v2"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_vm_infos"></a> [vm\_infos](#output\_vm\_infos) | Informations concernant l'instance |
